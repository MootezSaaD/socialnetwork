package entities;

public class PhoneNumber {
	private int countryCode;
	private long number;
	private String label;
	
	public PhoneNumber(int countryCode, long number, String label) {
		this.countryCode = countryCode;
		this.number = number;
		this.label = label;
	}
	
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
}
