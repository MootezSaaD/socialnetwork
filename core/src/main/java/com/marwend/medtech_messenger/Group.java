package com.marwend.medtech_messenger;

import java.util.ArrayList;

public class Group implements ContactItem {
	private String name;
	private ArrayList<ContactItem> Contacts;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<ContactItem> getContacts() {
		return Contacts;
	}
	public void setContacts(ArrayList<ContactItem> contacts) {
		Contacts = contacts;
	}
}
