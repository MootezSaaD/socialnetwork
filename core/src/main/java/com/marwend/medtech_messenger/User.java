package com.marwend.medtech_messenger;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class User implements ContactItem{
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private Date dateOfBirth;
	private LinkedList<PhoneNumber> phoneNumber;
	private LinkedList<Email> email;
	private URI photos;

	private ArrayList<ContactItem> Contacts;

	public User(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
	}
	
	public User(String firstName, String lastName, String username, String password, LinkedList<Email> email) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setUsername(username);
		this.setPassword(password);
	}

	
	public User(String firstName, String lastName, String username, String password, Date dateOfbirth, LinkedList<PhoneNumber> phoneNumber, LinkedList<Email> email, URI photos) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setUsername(username);
		this.setPassword(password);
		this.setDateOfBirth(dateOfbirth);
		this.setPhoneNumber(phoneNumber);
		this.setEmail(email);
		this.setPhotos(photos);
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public LinkedList<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(LinkedList<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public LinkedList<Email> getEmail() {
		return email;
	}
	public void setEmail(LinkedList<Email> email) {
		this.email = email;
	}
	
	public URI getPhotos() {
		return photos;
	}
	public void setPhotos(URI photos) {
		this.photos = photos;
	}
	
	public ArrayList<ContactItem> getContacts() {
		return Contacts;
	}
	public void setContacts(ArrayList<ContactItem> contacts) {
		Contacts = contacts;
	}

	public static void resetPassword(String newpassword){
		this.password.setPassword(newpassword);  
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}
