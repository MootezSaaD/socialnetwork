package com.marwend.medtech_messenger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;

import entities.Email;
import entities.PhoneNumber;
import entities.User;
import repository.UserRepository;

/**
 * Hello world!
 * 
 *
 */
public class App 
{
	
	static Scanner sc = new Scanner(System.in);
	static ArrayList<User> users = new ArrayList<User>();

    public static void main( String[] args )
    {
    	UserRepository userRepository = new UserRepository();
    	Date birthDate = null;
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
        LinkedList<Email> emails = new LinkedList<Email>();
    	
		while(birthDate == null) {
			try {
				birthDate = simpleDateFormat.parse("10-14-1997");
			} catch (ParseException e) {
				e.printStackTrace();
			}
    	}
		
        PhoneNumber phoneNumber = new PhoneNumber(216, 53977605, "Mobile");
        
        Email email = new Email("marwen.dallel@medtech.tn", "Business");
        emails.add(email);
        
    	User marwen = new User("Marwen", "Dallel", "marwen.dallel","password123");
    	userRepository.addUser(marwen);
    	marwen.addPhoneNumber(phoneNumber);
    	User boo = new User("Mootez", "Saad","mootez.saad","password321");
    	
    	{
    		LinkedList<PhoneNumber> ph = marwen.getPhoneNumber();
    		PhoneNumber ph2 = new PhoneNumber(216,55123321,"Business");
    		ph.add(ph2);
    		marwen.setPhoneNumber(ph);
    		
    	}
    	

    	users.add(marwen);
    	users.add(boo);
    	
    	while (true) {
            int userinput;
            System.out.println("1. Login");
            System.out.println("2. Register");
            System.out.print("Enter choice: ");
            try {
                userinput = sc.nextInt();
            } catch (Exception e) {
            	System.out.println("Enter a valid digit!");
            	continue;
            }
			if (userinput == 1) {
            	login();
            } else if (userinput == 2) {
            	register();
            	login();
            } else {
            	continue;
            }
    	}  
    }
    
    
    public static void dashboard() {
    	System.out.println("\n1. Add a new friend");
    	System.out.println("2. Set/modify birth date");
    	System.out.println("3. Set/modify phone number");
    	System.out.println("3. Set/modify password");

        System.out.println("3. Exit");

    }

    public static void resetPassword(){
        
    }
    public static void login() {
        System.out.print("\nEnter username: ");
        String username = sc.next();
        System.out.print("Enter password: ");
        String password = sc.next();
    	User user = new User(username, password);
        for(int i=0; i < users.size(); i++) {
        	if (users.get(i).equals(user)) {
        		dashboard();
        	} else {
        		System.out.println("Wrong credentials!");
        	}
        }
    }
    
    public static void register() {
    	System.out.print("\nEnter first name: ");
        String firstName = sc.next();

    	System.out.print("Enter last name: ");
        String lastName = sc.next();
        
        boolean confirm = false;
        String username = null;

        while (confirm==false) {
    		System.out.print("Enter username: ");
            username = sc.next();
            
            for(int i=0; i < users.size();) {
            	if (users.get(i).getUsername().equals(username)) {
            		System.out.println("Existing username! Try again.");
            		break;
            	} else {
            		confirm = true;
            		break;
            	}
            }
        };
        
        System.out.print("Enter email address: ");
        String address = sc.next();
        System.out.print("Enter email label (Business email, Personal email): ");
        String label = sc.next();

        Email email = new Email(address, label);
        
        LinkedList<Email> emails = new LinkedList<Email>();
        emails.add(email);
        
    	System.out.print("Enter password: ");
        String password = sc.next();
        
        // Add new user
        User newUser = new User(firstName, lastName, username, password, emails);
        users.add(newUser);
        
        dashboard();
    }

}