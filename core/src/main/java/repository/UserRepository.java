package repository;

import java.util.ArrayList;

import entities.User;

public class UserRepository {
	private ArrayList<User> users = new ArrayList<User>();
	
	public void addUser(User user) {
		this.users.add(user);
	}
	
	public void removeUser(User user) {
		this.users.remove(user);
	}
	
	public ArrayList<User> findByFirstName(String name) {
		ArrayList<User> results = new ArrayList<User>();
		for(int i=0; i<users.size(); i++) {
			if (name.equals(users.get(i).getFirstName())) {
				results.add(users.get(i));
			}
		}
		return results; 
	}
}
